function myClickHandler(event){
    // Fungsi untuk menyimpan detik terakhir setelah di pause

  if (this.paused()) {
    this.play();
    console.log("play lagi detik: ",this.currentTime());
  } else {
    this.pause();
    console.log("Paused di detik: ",this.currentTime());
    var lastPaused = {};
    lastPaused.time = this.currentTime();
    $.ajax(
      {
        url: "proses_php/add_last_pause2.php",
        method: "POST",
        data: lastPaused,
        success: function(res){
          console.log(res);
        }
      }
    );
  }
};
var time = 0;
  if (parseInt(document.querySelector('input[type="hidden"]').value) != 0  )
  {
    time = parseInt(document.querySelector('input[type="hidden"]').value);
  }
console.log(time);
var myPlayer = videojs('videoPlayer', {
    userActions: {
      // Sementara hanya berfungsi ketika klik sekali di sekitar video
      // Untuk klik pause/play di player belum menemukan cara untuk override nya untuk bisa menyimpan detik video
      click: myClickHandler
    }
  });
myPlayer.ready(function() {
    myPlayer.currentTime(time);
    console.log(!myPlayer.paused());
});

window.addEventListener('beforeunload', function (e) {
  myPlayer.pause();
  
  console.log("Paused di detik: ",myPlayer.currentTime());
  var lastPaused = {};
  lastPaused.time = myPlayer.currentTime();
  $.ajax(
      {
        url: "proses_php/add_last_pause2.php",
        method: "POST",
        data: lastPaused,
        success: function(res){
          console.log(res);
        }
      }
  );
  e.preventDefault();
  e.returnValue = '';
});
