<?php
    # Mengambil data halaman terakhir PDF dari mysql
    include "proses_php/koneksi_db.php";
    $query = mysqli_query($link, "SELECT * FROM pdf1");
    $data = mysqli_fetch_array($query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500&family=Roboto:wght@500;700;900&display=swap" rel="stylesheet"> 

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

    <!--Jquery untuk kirim data dengan ajax ke php -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Materi PDF 1</title>
</head>
<body>
     <!-- Navbar Start -->
     <nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top p-0">
        <a href="index.html" class="navbar-brand d-flex align-items-center border-end px-4 px-lg-5">
            <h2 class="m-0 text-primary">LIMAS</h2>
        </a>
        <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto p-4 p-lg-0">
                <a href="index.php" class="nav-item nav-link">Home</a>
                <a href="index.php" class="nav-item nav-link active">Material</a>
                <a href="" class="nav-item nav-link">Service</a>
                <a href="" class="nav-item nav-link">Project</a>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pages</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="" class="dropdown-item">Feature</a>
                        <a href="" class="dropdown-item">Free Quote</a>
                        <a href="" class="dropdown-item">Our Team</a>
                        <a href="" class="dropdown-item">Testimonial</a>
                        <a href="" class="dropdown-item">404 Page</a>
                    </div>
                </div>
                <a href="contact.html" class="nav-item nav-link">Contact</a>
            </div>
            <a href="" class="btn btn-primary rounded-0 py-4 px-lg-5 d-none d-lg-block">Hello, Mr A<i class="fa fa-user ms-3"></i></a>
        </div>
    </nav>
    <!-- Navbar End -->

    <!-- Kirim data halaman terakhir ke javascript -->
    <input type="hidden" value="<?= $data['last_page'] ?>">

    <!-- Navbar dari pdf viewer -->
    <div class="container my-2">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8 position-relative">
                    <button type="button" id="prev-page" class="btn btn-primary"><i class="fas fa-arrow-alt-circle-left"></i> Prev</button>
                        <span class="page-info position-absolute top-0 start-50 translate-middle-x">
                            Page <span id="page-num"></span> of <span id="page-count"></span>
                        </span>
                    <button type="button" id="next-page" class="btn btn-primary position-absolute top-0 end-0">Next <i class="fas fa-arrow-alt-circle-right"></i></button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>

    <!-- Tempat file pdf di render -->
    <div class="position-relative">
        <div class="position-absolute top-85 start-50 translate-middle-x">
            <canvas id="pdf-render"></canvas>
        </div>
    </div>

    <!-- JS untuk render pdf -->
    <script src="https://mozilla.github.io/pdf.js/build/pdf.js"></script>
    <script src="js/pdf-viewer.js"></script>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/isotope/isotope.pkgd.min.js"></script>
    <script src="lib/lightbox/js/lightbox.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>
</html>